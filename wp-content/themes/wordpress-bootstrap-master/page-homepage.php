<?php
/*
Template Name: Homepage
*/
?>

<?php get_header(); ?>
			


			<div id="content" class="clearfix row">
			
				<div id="main" class="col-sm-12 clearfix" role="main">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					
					<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">

						<header>

							<!-- FEATURED IMAGE -->
							<!-- FEATURED IMAGE -->
							<?php 
								$post_thumbnail_id = get_post_thumbnail_id(); $featured_src = wp_get_attachment_image_src( $post_thumbnail_id, 'wpbs-featured-home' ); 
							?>
							
							
								<img id="alien-large" src="<?php echo get_stylesheet_directory_uri(); ?>/images/alien-large.png" alt="">
							  
							  
							  <!-- <div id="red-eyes"></div> -->
								

						</header>
						

						
						<!-- CONTENT -->
						<!-- CONTENT -->
						<!-- CONTENT -->
						<section class="row post_content">
						
							<div class="col-sm-8">
								
								<!-- PAGE CONTENT -->
								<?php the_content(); ?>


						<!-- TENDING NEWS -->
						<!-- TENDING NEWS -->
						<div id="trending-news">
							<h1><span>AWL</span> News</h1>

							<!-- POST LOOP -->
							 <?php $args = 'numberposts=5&cat=-3,-6'; $lastposts = get_posts( $args ); foreach($lastposts as $post) : setup_postdata($post); ?>
								
								<div class="teaser-post-wrapper">
							        <?php if ( has_post_thumbnail() ) { ?>
											
										<div class="teaser-thumbnail">
											<?php the_post_thumbnail(); ?>
										</div>
									<?php } else { ?>
										<div class="teaser-thumbnail">
											<img src="<?php bloginfo('template_directory'); ?>/images/awl-no-pic.jpg" alt="<?php the_title(); ?>" />
										</div>
									<?php } ?>
										
										<!-- TITLE -->
										<h2 class="title"><a href="<?php the_permalink(); ?>"> <?php the_title(); ?> </a></h2>

									  	<!-- DATE -->
									  	<div class="date">
							        		<?php the_time('F jS, Y') ?> | <?php the_views(); ?> | <a href="<?php the_permalink(); ?>/#respond" title=""><?php comments_number(''); ?></a>		
										</div>
										
										<!-- POST CONTENT -->
								       <div class="content">
							        		<?php the_excerpt() ;?>
							        	</div>
					        	</div>
					        	<!-- END TEASER POST WRAPPER -->

										 
							<?php endforeach; ?>
							<!-- END POST LOOP -->
							<!-- END POST LOOP -->

						</div>

							</div>
							
							<?php get_sidebar('sidebar2'); // sidebar 2 ?>
													
						</section> <!-- end article header -->
						
						<footer>
							<p class="clearfix"><?php the_tags('<span class="tags">' . __("Tags","wpbootstrap") . ': ', ', ', '</span>'); ?></p>
						</footer> <!-- end article footer -->

					</article> <!-- end article -->
					
					<?php 
						// No comments on homepage
						//comments_template();
					?>
					
					<?php endwhile; ?>	
					
					<?php else : ?>
					
					<article id="post-not-found">
					    <header>
					    	<h1><?php _e("Not Found", "wpbootstrap"); ?></h1>
					    </header>
					    <section class="post_content">
					    	<p><?php _e("Sorry, but the requested resource was not found on this site.", "wpbootstrap"); ?></p>
					    </section>
					    <footer>
					    </footer>
					</article>
					
					<?php endif; ?>
			
				</div> <!-- end #main -->
    
				<?php //get_sidebar(); // sidebar 1 ?>
    
			</div> <!-- end #content -->

<?php include('partials/home_scripts.php'); ?>

<?php get_footer(); ?>